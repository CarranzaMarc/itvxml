function pintarHoras() {
  
  var dhora = new Date();
  dhora.setHours(8);
  dhora.setMinutes(0);
   
  var myJSON = $("#horas_llenas").text(); 
  var horas_llenas = JSON.parse(myJSON);
  var table = "";

  for (i = 0; i < 6; i++) {
    table += "<tr>";
    for (j = 0; j < 4; j++) {
      var hora = dhora.getHours();
      var minutes = dhora.getMinutes();
      if(minutes === 0) {
        minutes = "00";
      }

      if (hora < 10) {
        hora = "0" + hora;
      }
      
      var horaMin = hora + ":" + minutes;
      var horaSec = horaMin + ":00";
      var dataActual = new Date();
      
      var horaActual = dataActual.getHours();
      var minActual = dataActual.getMinutes() + 15;
      var secActual = dataActual.getSeconds();
      
      if(horaActual < 10){
        horaActual = "0" + horaActual;
      }
      if(minActual < 10){
        minActual = "0" + minActual;
      }
      if(secActual < 10){
        secActual = "0" + secActual;
      }
      
      var hmsActual = horaActual + ":" + minActual + ":" + secActual;
      var disp = 0;
      console.log(hmsActual);
      console.log(horaSec);
      
       diahoy = dataActual.getDate();
       anohoy = dataActual.getFullYear();
       meshoy = dataActual.getMonth() + 1;
       meshoyform = "0" + meshoy;
       diahoyform = "0" + diahoy;

       if(diahoy < 10){
        if(meshoy < 10){
            fecha_hoy = anohoy + "-" + meshoyform + "-" + diahoyform;
        } else {
          fecha_hoy = anohoy + "-" + meshoy + "-" + diahoyform;
        }
       } else {
        if(meshoy < 10){
          fecha_hoy = anohoy + "-" + meshoyform + "-" + diahoy;                 
        } else {
          fecha_hoy = anohoy + "-" + meshoy + "-" + diahoy;
        }
       }
      
      var fecha_hora = document.querySelector("#fecha").textContent;
      
      if(hmsActual > horaSec && fecha_hoy === fecha_hora){
          disp = 2;
         }
      
      for(k = 0; k < horas_llenas.length; k++){
        if (horaSec == horas_llenas[k]) {
          disp++;
        }
      }
      
      if (j % 2 === 0) {
        dhora.setMinutes(30);
      } else {
        dhora.setHours(parseInt(hora) + 1);
        dhora.setMinutes(0);
      }
      if (disp === 0) {
        table += "<td><button class='container__hora container__disponible'>" + horaMin + "</button></td>";
      } else if (disp === 1) {
        table += "<td><button class='container__hora container__mediodisponible'>" + horaMin + "</button></td>";
      } else {
        table += "<td><button class='container__hora container__nodisponible'>" + horaMin + "</button></td>";
        
      }
    }
    table += "</tr>";
  }
  
  table+="<tr><td colspan='4'><a href='calendari.php'><button class='container__btn'>Tornar al calendari</button></a></td></tr>";
  
  $(".container__table").append(table);

}