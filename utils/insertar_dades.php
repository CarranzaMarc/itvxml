<?php
  require_once 'login.php';
  require_once 'errors.php';
  require_once 'validate.php';

  session_start();
  //camps taula CLIENTES
  $nom = validate_data($_POST['nom']);
  $cognoms = validate_data($_POST['cognoms']);
  $email = validate_data($_POST['email']);
  $telefon = validate_data($_POST['telefon']);
  //camps taula VEHICULOS
  $matricula = $_SESSION['matricula'];
  $tipus = 'coche';

  if((isset($_SESSION['matricula'])) && (isset($_SESSION['fecha'])) && (isset($_SESSION['hora'])) && (isset($_SESSION['centro']))){
    $db_server = mysqli_connect($db_hostname, $db_username, $db_password, $db_database);
    if (!$db_server){
      obtener_error(0);
    } 
    mysqli_select_db($db_server, $db_database) or die("Unable to select database: " . mysqli_error());

    //query per esborrar qualsevol cita amb la matricula introduida abans de modificar-la
    $query = "DELETE FROM citas WHERE matricula LIKE '".$_SESSION['matricula']."';"; 
    $result = mysqli_query($db_server, $query);

    //query per esborrar el vehicle amb la matricula introduida
     $query = "DELETE FROM vehiculos WHERE matricula LIKE '".$_SESSION['matricula']."';"; 
     $result = mysqli_query($db_server, $query);

    //query per esborrar el client amb les dades introduides
     $query = "DELETE FROM clientes WHERE email LIKE '".$email."';"; 
     $result = mysqli_query($db_server, $query);

    //insert a la taula CLIENTES
    $query = "INSERT INTO clientes VALUES ('".$email."', '".$nom."', '".$cognoms."', '".$telefon."');"; 
    $result = mysqli_query($db_server, $query);
    if (!$result){
      obtener_error(3);
    } 

    $query = "INSERT INTO vehiculos VALUES ('".$_SESSION['matricula']."', '".$tipus."', '".$email."');"; 
    $result = mysqli_query($db_server, $query);
    if (!$result){
      obtener_error(3);
    } 

    $query = "INSERT INTO citas (id, fecha, hora, matricula, id_centro) VALUES (NULL, '".$_SESSION['fecha']."', '".$_SESSION['hora']."', '".$_SESSION['matricula']."', 1);"; 
    $result = mysqli_query($db_server, $query);
    //si no es pot fer l'insert ens mostra una pantalla d'error
    if (!$result) {
     $query = "DELETE FROM vehiculos WHERE matricula LIKE '".$_SESSION['matricula']."';"; 
     $result = mysqli_query($db_server, $query);
     $query = "DELETE FROM clientes WHERE email LIKE '".$email."';"; 
     $result = mysqli_query($db_server, $query);
     obtener_error(1);     
    }
    //si el pot fer, ens mostra la informacio de la cita
    else{
      header("Location: ../htmls/informacio.php");
    }
   }
   else{
       obtener_error(2);     
   }
  mysqli_close($db_server);
?>