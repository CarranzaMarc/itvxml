<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Admin ITV</title>
  <link rel="stylesheet" href="../css/normalize.css">
  <link rel="stylesheet" href="../css/adminbody.css">
  <link rel="stylesheet" href="../css/adminclient.css">
  <link rel="icon" href="../imgs/favicon.png">
  <script src="../js/jquery.js"></script>
  <script src="../js/calendario.js"></script>
  <script src="../sal/sweetalert2.all.min.js"></script>
  <link rel="stylesheet" href="../sal/sweetalert2.min.css">
</head>

<body>
  <?php
    require_once('../js/adminheader.php');
  ?>
  <?php
    require_once('../js/adminnav.php');
  ?>
  <section class="container">
      <div class="container__objects">
        <div class="container__wrapper">
           <?php
              require_once '../utils/login.php';
              require_once '../utils/validate.php';
              $client = validate_data($_POST['client']);
              $db_server = mysqli_connect($db_hostname, $db_username, $db_password, $db_database);
              if (!$db_server) die("Unable to connect to MySQL: " . mysql_error());    
              mysqli_select_db($db_server, $db_database) or die("Unable to select database: " . mysqli_error());
              $query = "SELECT nombre, apellidos, telefono FROM clientes WHERE email LIKE '".$client."';"; 
              $result = mysqli_query($db_server, $query);
              if (!$result) die ("Database access failed: " . mysql_error()); 
              $rows = mysqli_num_rows($result);
              if($rows){
                echo "<table class='container__calendari'><tr class='container__thead'><th>Nom</th><th>Cognom</th><th>Telèfon</th><th>Data i hora cita</th></tr>";
                for ($i = 0 ; $i < $rows ; $i++){
                  $consulta = mysqli_fetch_assoc($result);
                  echo "<tr>";
                  foreach ($consulta as $key => $valor) {
                    echo "<td>".$valor."</td>";
                  }
                }
                $query = "SELECT fecha, hora FROM citas WHERE matricula LIKE (SELECT matricula FROM vehiculos WHERE email_cliente LIKE '".$client."');";
                $result = mysqli_query($db_server, $query);
                if (!$result) die ("Database access failed: " . mysql_error()); 
                $rows = mysqli_num_rows($result);
                for ($i = 0 ; $i < $rows ; $i++){
                  $consulta = mysqli_fetch_assoc($result);
                  echo "<td class='container__cita'>";
                  foreach ($consulta as $key => $valor) {
                    echo $valor." ";
                  }
                  echo "</td>";
                }          
                echo "</tr>"; 
                echo "</table>";
              }
              else{
                echo "<p>No hi ha cap client amb aquest correu.<p>";
              }
              mysqli_close($db_server);
            ?>
            <a href="adminpanell.php">
              <button class="container__btnback">
                Tornar
              </button>
            </a>
        </div>
      </div>
 
    </section>
</body>

</html>