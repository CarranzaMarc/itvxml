<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>IAM ITV</title>
  <link rel="stylesheet" href="../css/normalize.css">
  <link rel="stylesheet" href="../css/body.css">
  <link rel="stylesheet" href="../css/gestio.css">
  <link rel="icon" href="../imgs/favicon.png">
</head>

<body>
  <?php
    require_once('../js/header.php');
    session_start();
  ?>
    <section class="container">
      <div class="container__objects">
        <div class="container__data">
          <h1 class="container__title">
            RESERVA
          </h1>
          <table class="container__table">
            <tr>
              <td>Data:</td>
              <td class="container__important"><?php echo $_SESSION['fecha']; ?></td>
            </tr>
            <tr>
              <td>Hora:</td>
              <td class="container__important"><?php echo $_SESSION['hora']; ?></td>
            </tr>
            <tr>
              <td>Matricula:</td>
              <td class="container__important"><?php echo $_SESSION['matricula']; ?></td>
            </tr>
            <tr>
              <td>Centre:</td>
              <td class="container__important"><?php echo $_SESSION['centro']; ?></td>
            </tr>
            <tr>
              <td class="container__btns" colspan="2">
                <a href="../index.php">
                  <input type="button" class="container__btn" value="Tornar">
                </a>
              </td>
            </tr>
          </table>
        </div>
      </div>
    </section>
    <?php
    require_once('../js/footer.php');
  ?>
</body>

</html>