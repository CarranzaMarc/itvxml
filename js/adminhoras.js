$(function() {
  
  var tablaAdmin = (function(){
    var printString = "";
    var printString2 = "";
    var printString3 = "";
    function pintarCabecera(){
      printString += "<tr class='container__thead'><th>HORA</th><th>L1</th><th>L2</th></tr>";
      printString2 += "<tr class='container__thead'><th>HORA</th><th>L1</th><th>L2</th></tr>";
      printString3 += "<tr class='container__thead'><th>HORA</th><th>L1</th><th>L2</th></tr>";
    }
    
    function pintarHoras(){
      
      var myJSON = $("#horas_ocupadas").text(); 
      var horas_ocupadas = JSON.parse(myJSON);
      console.log(horas_ocupadas);
      
      var dhora = new Date();
      dhora.setHours(8);
      dhora.setMinutes(0);
      
      var hora, minutes, horaMin, horaSec, hora1, H1, hora2, H2;
      var x, z, printed;
      
      for (j = 0; j < 24; j++) {
      if(j < 8){
      printString += "<tr>"
        
        hora = dhora.getHours();
        minutes = dhora.getMinutes();
        if(minutes === 0) {
          minutes = "00";
        }
        
        if (hora < 10) {
          hora = "0"+hora;
        } 
        
        horaMin = hora + ":" + minutes;
        horaSec = horaMin + ":00";
        
        if (j % 2 === 0) {
          dhora.setMinutes(30);
        } else {
          dhora.setHours(parseInt(hora) + 1);
          dhora.setMinutes(0);
        }
        
        hora1 = "";
        H1 = false;
        hora2 = "";
        H2 = false;
        
        printString += "<td class='container__hora'>" + horaMin + "</td>";
       
        // Recorrem l'array de hores ocupades en busca d'una hora que coincideixi amb la que s'està imprimint.
        for (x = 0; x < horas_ocupadas.length; x+=2) {
          
          // Si una hora coincideix, es guarda en la variable hora1 la matrícula relacionada a aquella hora i diem que H1 està definida (hora1).
          
          if (horaSec === horas_ocupadas[x]) {
            hora1 = horas_ocupadas[x+1];
            H1 = true;
            
            // Recorrem un altre vegada l'array en busca d'una segona cita a la mateixa hora
            
            for (z = 0; z < horas_ocupadas.length; z+=2) {
              
              // Si la trobem. guardem a la variable hora2 la matricula relacionada a aquella hora i diem que H2 està definida (hora2).
              
              if (horaSec === horas_ocupadas[z] && horas_ocupadas[z+1] !== horas_ocupadas[x+1]){
                hora2 = horas_ocupadas[z+1];
                H2 = true;
                
              }
            }           
          }
        }

        printed = false;
        if (H2) {
            printString+= "<td class='hora__ocupada'> " + hora1 + " </td>";  
            printString+= "<td class='hora__ocupada'> " + hora2 + " </td>";  
            printed = true;
          } else if(H1) {
            printString+= "<td class='hora__ocupada'> " + hora1 + " </td>";  
            printString+= "<td class='hora__nodisponible'> LIBRE </td>";
            printed = true;
          } else {
            printString+= "<td class='hora__nodisponible'> LIBRE </td>";
            printString+= "<td class='hora__nodisponible'> LIBRE </td>";
          }     
        }else if(j < 16){
        printString2 += "<tr>"
        
        hora = dhora.getHours();
        minutes = dhora.getMinutes();
        if(minutes === 0) {
          minutes = "00";
        }
        
        horaMin = hora + ":" + minutes;
        horaSec = horaMin + ":00";
        
        if (j % 2 === 0) {
          dhora.setMinutes(30);
        } else {
          dhora.setHours(parseInt(hora) + 1);
          dhora.setMinutes(0);
        }
        
        hora1 = "",
            H1 = false,
            hora2 = "",
            H2 = false;
        
        printString2 += "<td>" + horaMin + "</td>";
       
        // Recorrem l'array de hores ocupades en busca d'una hora que coincideixi amb la que s'està imprimint.
        
        for (x = 0; x < horas_ocupadas.length; x+=2) {
          
          // Si una hora coincideix, es guarda en la variable hora1 la matrícula relacionada a aquella hora i diem que H1 està definida (hora1).
          
          if (horaSec === horas_ocupadas[x]) {
            hora1 = horas_ocupadas[x+1];
            H1 = true;
            
            // Recorrem un altre vegada l'array en busca d'una segona cita a la mateixa hora
            
            for (z = 0; z < horas_ocupadas.length; z+=2) {
              
              // Si la trobem. guardem a la variable hora2 la matricula relacionada a aquella hora i diem que H2 està definida (hora2).
              
              if (horaSec === horas_ocupadas[z] && horas_ocupadas[z+1] !== horas_ocupadas[x+1]){
                hora2 = horas_ocupadas[z+1];
                H2 = true;
                
              }
            }           
          }
        }

        printed = false;
        if (H2) {
            printString2+= "<td class='hora__ocupada'> " + hora1 + " </td>";  
            printString2+= "<td class='hora__ocupada'> " + hora2 + " </td>";  
            printed = true;
          } else if(H1) {
            printString2+= "<td class='hora__ocupada'> " + hora1 + " </td>";  
            printString2+= "<td class='hora__nodisponible'> LIBRE </td>";
            printed = true;
          } else {
            printString2+= "<td class='hora__nodisponible'> LIBRE </td>";
            printString2+= "<td class='hora__nodisponible'> LIBRE </td>";
          }   
        }else{
        printString3+= "<tr>";
        
        hora = dhora.getHours();
        minutes = dhora.getMinutes();
        if(minutes === 0) {
          minutes = "00";
        }
        
        horaMin = hora + ":" + minutes;
        horaSec = horaMin + ":00";
        
        if (j % 2 === 0) {
          dhora.setMinutes(30);
        } else {
          dhora.setHours(parseInt(hora) + 1);
          dhora.setMinutes(0);
        }
        
        hora1 = "",
            H1 = false,
            hora2 = "",
            H2 = false;
        
        printString3 += "<td>" + horaMin + "</td>";
       
        // Recorrem l'array de hores ocupades en busca d'una hora que coincideixi amb la que s'està imprimint.
        
        for (x = 0; x < horas_ocupadas.length; x+=2) {
          
          // Si una hora coincideix, es guarda en la variable hora1 la matrícula relacionada a aquella hora i diem que H1 està definida (hora1).
          
          if (horaSec === horas_ocupadas[x]) {
            hora1 = horas_ocupadas[x+1];
            H1 = true;
            
            // Recorrem un altre vegada l'array en busca d'una segona cita a la mateixa hora
            
            for (z = 0; z < horas_ocupadas.length; z+=2) {
              
              // Si la trobem. guardem a la variable hora2 la matricula relacionada a aquella hora i diem que H2 està definida (hora2).
              
              if (horaSec === horas_ocupadas[z] && horas_ocupadas[z+1] !== horas_ocupadas[x+1]){
                hora2 = horas_ocupadas[z+1];
                H2 = true;
                
              }
            }           
          }
        }

        printed = false;
        if (H2) {
            printString3+= "<td class='hora__ocupada'> " + hora1 + " </td>";  
            printString3+= "<td class='hora__ocupada'> " + hora2 + " </td>";  
            printed = true;
          } else if(H1) {
            printString3+= "<td class='hora__ocupada'> " + hora1 + " </td>";  
            printString3+= "<td class='hora__nodisponible'> LIBRE </td>";
            printed = true;
          } else {
            printString3+= "<td class='hora__nodisponible'> LIBRE </td>";
            printString3+= "<td class='hora__nodisponible'> LIBRE </td>";
          }  
        }
      }
      
      printString3 += "</tr>";
    }
    
    function init(){
      pintarCabecera();
      pintarHoras();
      $("#printHere").append(printString);
      $("#printHere2").append(printString2);
      $("#printHere3").append(printString3);
    }

    return {
      init: init
    }
  })();
  
  tablaAdmin.init();
  
});