<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>IAM ITV</title>
  <link rel="stylesheet" href="../css/normalize.css">
  <link rel="stylesheet" href="../css/body.css">
  <link rel="stylesheet" href="../css/hora.css">
  <link rel="icon" href="../imgs/favicon.png">
  <script src="../js/jquery.js"></script>
  <script src="../js/hora.js"></script>
  
   
</head>

<body>
  <?php
  session_start();
  if(isset($_POST["fecha"])){
    $_SESSION["fecha"] = $_POST["fecha"];
  }
  else{
      header('Location: ../index.php');
  }
//$date=date_create_from_format("Y-m-d","2017-12-11");
//echo date_format($date,"D d M Y");
    
  require_once '../utils/login.php';

  $db_server = mysqli_connect($db_hostname, $db_username, $db_password, $db_database);
  if (!$db_server) die("Unable to connect to MySQL: " . mysql_error());    

  mysqli_select_db($db_server, $db_database) or die("Unable to select database: " . mysqli_error());
  
   $query = "SELECT hora FROM citas WHERE fecha LIKE '".$_SESSION['fecha']."' AND id_centro = 1";
    $result = mysqli_query($db_server, $query);
    if (!$result) die ("Database access failed: " . mysql_error());
    $rows = mysqli_num_rows($result);
    $json = array();
    for ($i = 0 ; $i < $rows ; $i++){
      $consulta = mysqli_fetch_assoc($result);
      foreach ($consulta as $key => $valor) 
        $json[] = $valor;
    }

    $json = json_encode($json);
    print "<div class='hide' id='horas_llenas'>" . $json . "</div>";
    print "<div class='hide' id='fecha'>" . $_SESSION["fecha"] . "</div>";
  
  
  
  ?>
  <?php
      require_once('../js/header.php');
  ?>
  <section class="container">
    <div class="container__objects">
      <div class="container__fecha">
        <p class="container__data">
          <?php
            require_once('../utils/traducir_fecha.php');
            echo fechaCatalan($_SESSION["fecha"]);
          ?>
        </p>
        <p class="container__data">
          <?php echo $_SESSION["matricula"]; ?>
        </p>
      </div>
      <table class="container__table">
      </table>
      <form id="hora_escogida" class="hide" action="confirmacio.php" method="POST">
        <input type="text" name="hora">
      </form>
    </div>
  </section>
  <?php
      require_once('../js/footer.php');
  ?>
  
  <script>

    $(function() {
      

      pintarHoras();
      
      $(".container__disponible, .container__mediodisponible").click(function(){
        var hora = $(this).text();
        var horaform = hora + ":00";

        console.log("clicks done");

        $('input[name="hora"]').val(horaform);
        $("#hora_escogida").submit();
      });
      
    });
  </script>
</body>

</html>