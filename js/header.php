<?php
  $header = "<header class='header'>";
  $header .= "<figure class='header__figure'>";
  $header .= "<a href='../index.php'>";
  $header .= "<img class='header__logo' src='../imgs/logo.png'></a></figure>";
  $header .= "<nav class='nav'>";
  $header .= "<ul class='nav__list'>";
  $header .= "<li class='nav__element'><a href='#'>Cita</a></li>";
  $header .= "<li class='nav__element'><a href='#'>Nosaltres</a></li>";
  $header .= "<li class='nav__element'><a href='#'>FAQ</a></li>";
  $header .= "</ul></nav></header>";
  echo $header;
?>