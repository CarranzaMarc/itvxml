  <?php
  require_once '../utils/login.php';
  require_once '../utils/errors.php';
  require_once '../utils/validate.php';

  session_start();
  if(isset($_POST['matricula'])){
    $_SESSION['matricula'] = validate_data($_POST['matricula']);
  }

  $db_server = mysqli_connect($db_hostname, $db_username, $db_password, $db_database);
  if (!$db_server){
    obtener_error(0);
  }    
  mysqli_select_db($db_server, $db_database) or die("Unable to select database: " . mysqli_error());

  $fecha_actual = date("Y-m-d");
  $_SESSION['fechactual'] = $fecha_actual;

  //es comprova si existeix alguna cita amb aquesta matricula i que la data sigui posterior a l'actual
  $query = "SELECT matricula FROM citas WHERE fecha >= '".$fecha_actual."'"; 
  $result = mysqli_query($db_server, $query);
  if (!$result) die ("Database access failed: " . mysql_error());

  $rows = mysqli_num_rows($result);
  for ($i = 0 ; $i < $rows ; $i++){
    $consulta = mysqli_fetch_assoc($result);
    foreach ($consulta as $key => $valor) {
      //si existeix una cita amb aquesta matricula es redirigeix a gestio.php
      if($valor==$_SESSION['matricula']){
        $exists = 1;
      }
    }
  }

  if($exists){
    //IMPORTANTE!!! cambiar la URL por la de vuestro server
    /*QUERY para seleccionar la fecha de la cita que existe con esa matricula*/
    $query = "SELECT fecha FROM citas WHERE matricula LIKE '".$_SESSION['matricula']."'"; //creació de la query
    $result = mysqli_query($db_server, $query);
    if (!$result) die ("Database access failed: " . mysql_error()); 
    $rows = mysqli_num_rows($result);
    for ($i = 0 ; $i < $rows ; $i++){
      $consulta = mysqli_fetch_assoc($result);
      foreach ($consulta as $key => $valor) {
        $_SESSION['fecha'] = $valor;
      }
    }

    /*QUERY para seleccionar la hora de la cita que existe con esa matricula*/
    $query = "SELECT hora FROM citas WHERE matricula LIKE '".$_SESSION['matricula']."'"; //creació de la query
    $result = mysqli_query($db_server, $query);
    if (!$result) die ("Database access failed: " . mysql_error()); 
    $rows = mysqli_num_rows($result);
    for ($i = 0 ; $i < $rows ; $i++){
      $consulta = mysqli_fetch_assoc($result);
      foreach ($consulta as $key => $valor) {
        $_SESSION['hora'] = $valor;
      }
    }
    /*QUERY para seleccionar el nombre del centro de la cita que existe con esa matricula*/
    $query = "SELECT nombre FROM centros WHERE id LIKE (SELECT id_centro FROM citas WHERE matricula LIKE '".$_SESSION['matricula']."')"; //creació de la query
    $result = mysqli_query($db_server, $query);
    if (!$result) die ("Database access failed: " . mysql_error()); 
    $rows = mysqli_num_rows($result);
    for ($i = 0 ; $i < $rows ; $i++){
      $consulta = mysqli_fetch_assoc($result);
      foreach ($consulta as $key => $valor) {
        $_SESSION['centro'] = $valor;
      }
    }
    header("Location: ../htmls/gestio.php");
    die();
    }
  //si NO existeix una cita amb aquesta matricula es redirigeix a calendari.php
  else{
    if(isset($_POST['matricula'])){
      $_SESSION['matricula'] = validate_data($_POST['matricula']);
    }
    //si hay más centros modificar con la variable $_POST['centro'];
    $_SESSION['centro'] = "Motors Ausias March";
    header("Location: ../htmls/calendari.php");
  }
  mysqli_close($db_server);
?>
