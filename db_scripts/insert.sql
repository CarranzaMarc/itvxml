USE ITV; 

INSERT INTO centros (id, nombre, direccion, nlineas_coche, nlineas_moto) VALUES (NULL, 'Maquinista', 'Carrer Potosi 22', 2, 3);
INSERT INTO centros (id, nombre, direccion, nlineas_coche, nlineas_moto) VALUES (NULL, 'Motors Ausias March', 'Avinguda Esplugues 33', 2, 0);

INSERT INTO clientes VALUES ('abc@iam.cat', 'abaca', 'perez sanchez', '600359514');
INSERT INTO clientes VALUES ('def@iam.cat', 'diefeno', 'rodriguez garcia', '123456789');
INSERT INTO clientes VALUES ('ghi@iam.cat', 'gehiro', 'lopez ruiz', '600359514');
INSERT INTO clientes VALUES ('jkl@iam.cat', 'jakala', 'sanchez', '123456789');
INSERT INTO clientes VALUES ('mno@iam.cat', 'minino', 'alhambra', '600359514');
INSERT INTO clientes VALUES ('pqr@iam.cat', 'poquer', 'paredes', '123456789');

INSERT INTO vehiculos VALUES ('1234ABC', 'coche', 'abc@iam.cat');                    
INSERT INTO vehiculos VALUES ('1234DEF', 'coche', 'def@iam.cat');
INSERT INTO vehiculos VALUES ('1234GHI', 'coche', 'ghi@iam.cat');                    
INSERT INTO vehiculos VALUES ('1234JKL', 'coche', 'jkl@iam.cat');
INSERT INTO vehiculos VALUES ('1234MNO', 'coche', 'mno@iam.cat');                    
INSERT INTO vehiculos VALUES ('1234PQR', 'coche', 'pqr@iam.cat');

INSERT INTO citas (id, fecha, hora, matricula, id_centro) VALUES (NULL, '2017-12-28', '13:30:00', '1234ABC', 2);
INSERT INTO citas (id, fecha, hora, matricula, id_centro) VALUES (NULL, '2018-01-11', '15:30:00', '1234DEF', 1);
INSERT INTO citas (id, fecha, hora, matricula, id_centro) VALUES (NULL, '2018-01-08', '19:30:00', '1234GHI', 2);
INSERT INTO citas (id, fecha, hora, matricula, id_centro) VALUES (NULL, '2018-01-11', '14:00:00', '1234JKL', 1);
INSERT INTO citas (id, fecha, hora, matricula, id_centro) VALUES (NULL, '2018-01-11', '15:30:00', '1234MNO', 2);
INSERT INTO citas (id, fecha, hora, matricula, id_centro) VALUES (NULL, '2017-12-30', '14:00:00', '1234PQR', 1);