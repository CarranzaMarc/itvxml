<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>IAM ITV</title>
  <link rel="stylesheet" href="../css/normalize.css">
  <link rel="stylesheet" href="../css/body.css">
  <link rel="stylesheet" href="../css/calendari.css">
  <link rel="icon" href="../imgs/favicon.png">
  <script src="../js/jquery.js"></script>
  <script src="../js/calendario.js"></script>
    <?php
    require_once '../utils/login.php';
    session_start();
    if(!isset($_SESSION['matricula'])){
      header('Location: ../index.php');
    }
  $db_server = mysqli_connect($db_hostname, $db_username, $db_password, $db_database);
  if (!$db_server) die("Unable to connect to MySQL: " . mysql_error());    

  mysqli_select_db($db_server, $db_database) or die("Unable to select database: " . mysqli_error());
  
   $query = "SELECT fecha FROM citas_llenas";
    $result = mysqli_query($db_server, $query);
    if (!$result) die ("Database access failed: " . mysql_error());
    $rows = mysqli_num_rows($result);
    $json = array();
    for ($i = 0 ; $i < $rows ; $i++){
      $consulta = mysqli_fetch_assoc($result);
      foreach ($consulta as $key => $valor) 
        $json[] = $valor;
    }

    $json = json_encode($json);
    print "<div class='hide' id='citas_llenas'>" . $json . "</div>";
//     setcookie("citas_llenas", $json, time()+3600, "/");
  ?>
</head>

<body>
    <?php
      require_once('../js/header.php');
    ?>
    <section class="container">
      <div class="container__objects">
        <div class="container__wrapper">
          <button type="button" id="retroceder" class="container__anterior"><</button>
          <div id="calendar" class="container__calendar container__fill"></div>
          <form method="POST" action="hora.php" id="fecha_escogida">
            <input type="text" name="fecha">
          </form>
          <button type="button" id="avanzar" class="container__seguent">></button>
        </div>
      </div>
    </section>
    <?php
      require_once('../js/footer.php');
    ?>
    <script>
      var f = new Date();
      var dia = f.getDate();
      var mes = f.getMonth();
      var mesInicial = mes;
      var ano = f.getFullYear();
      var anoInicial = ano;
      
 
             
      $("#fecha_escogida").hide();
      
      $("#calendar").html(Calendario.pintarCalendario(dia, mes, ano));
      setClicks();
      $("#retroceder").hide();

      $("#retroceder").click(function(){
        $("#avanzar").show();
        $("#calendar").removeClass("container__fill");
        $("#calendar").removeClass("container__fill2");
        mes--;
        if(mes<0){
          mes=11;
          ano--;
        }
        $("#calendar").html(Calendario.pintarCalendario(dia, mes, ano));
        if(mes==f.getMonth()){
           $("#retroceder").hide();
           $("#calendar").addClass("container__fill");
        }
        setClicks();
      });
       
      $("#avanzar").click(function(){
        $("#retroceder").show();
        $("#calendar").removeClass("container__fill");
        mes++;
        if(mes>11){
          mes=0;
          ano++;
        }
        $("#calendar").html(Calendario.pintarCalendario(dia, mes, ano));
        if(ano == parseInt(anoInicial)+1 && mes == mesInicial){
           $("#avanzar").hide();
           $("#calendar").addClass("container__fill2");
        }
        
        setClicks();
      });

      function setClicks(){
        $(".container__disponible").click(function(){
          var fecha = ano + "-" + (mes+1) + "-" + $(this).text();
          var diaform = $(this).text();
          
          if(diaform < 10){
            if(mes < 10){
              var fecha = ano + "-" + "0" + (mes+1) + "-" + "0" + diaform;
             } else {
              var fecha = ano + "-" + (mes+1) + "-" + "0" + diaform;
             }
           } else {
            if(mes < 10){
              var fecha = ano + "-" + "0" + (mes+1) + "-" + diaform;
             } else {
              var fecha = ano + "-" + (mes+1) + "-" + diaform;
             }
           }
                    
          $('input[name="fecha"]').val(fecha);
          $("#fecha_escogida").submit();
        })
      }           
    </script>
</body>

</html>
