<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>IAM ITV</title>
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/body.css">
  <link rel="stylesheet" href="css/index.css">
  <link rel="icon" href="imgs/favicon.png">
  <script src="js/jquery.js"></script>
  <script src="js/eggos.js"></script>
</head>

<body>
  <header class="header">
    <figure class="header__figure">
      <a href="index.php">
      <img class="header__logo" src="imgs/logo.png">
      </a>
    </figure>
    <nav class="nav">
      <ul class="nav__list">
        <li class="nav__element"><a href="#">Cita</a></li>
        <li class="nav__element"><a href="#">Nosaltres</a></li>
        <li class="nav__element"><a href="#">FAQ</a></li>
      </ul>
    </nav>
  </header>
  <section class="container">
    <div class="container__objects">
      <h1 class="container__title">
        CITA PRÈVIA ITV
      </h1>
          <form class="itv-form" method='POST' action='utils/comprovar_cita.php'>
            <table>
              <tr>
                <td class="itv-form__inputs">
                  <input type="text" class="itv-form__matricula" placeholder="Matricula" name='matricula'>
                  <button type="submit" class="itv-form__cita" id="submit">
                        <a>Demanar cita</a>
                  </button>
                </td>
              </tr>
            </table>
          </form>
    </div>
  </section>
  <?php
    require_once('js/footer.php');
  ?>
</body>
<script>  
  
    $("input[type=text]").keyup(function(){
      $(this).val($(this).val().toUpperCase() );
    });
  
  	$('input[name="matricula"]').focus();
    if($('input[name="matricula"]').is(':empty')){
      $('#submit').prop('disabled', true);
    }
    var error = false;
    $('input[name="matricula"]').on('input', function(){
    var exprega = /^(\d{4}[^AEIOUaeiou]{3})$/;

    //var expregb = /^([A-Z]{1,2}\d{4}[A-Z]{2})$/;
    if($(this).val().search(exprega)){  
      $(this).css("border", "2px solid red");
      $(this).css("background", "rgba(255, 0, 0, 0.5)");
      
      error = true;
      $('#submit').prop('disabled', true);
    }
    else {
      $(this).css("border",  "");
      $(this).css("background", "");
      
      $('#submit').prop('disabled', false);
    }
  });
  
</script>
</html>

