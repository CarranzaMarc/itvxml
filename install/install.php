<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="css/install.css">
  <script src="jquery.js"></script>
  <title>Manual d'instal·lació de l'aplicació Motors Ausias March</title>
</head>
<body>
  
    <section class="main">
      <div class="title">
        <div class="title__text">
          <h1>Manual d'instal·lació de l'aplicació Motors Ausias March</h1>
        </div>
        <img src="img/logo.png" alt="Logo IAM">
      </div>
      <div class="container__wrapper">
<!--         <div class="img__container">
          <img src="img/captura.png">
        </div> -->
        <div class="text__container">
          <h2>Passos a seguir per instal·lar l'aplicació</h2>
            <ol>
              <li>Crear la base de dades buida al panell de control</li>
              <li>
                Connectar-se per ssh a l’usuari del hosting.
                <span><p>ssh nomusuari@labs.iam.cat</p><button>Copiar</button></span> 
              </li>
              <li>
                Descarregar el fitxer d’instal·lació i modificar els permisos.
                <span><p>curl -O https://bitbucket.org/CarranzaMarc/itvxml/raw/master/install.sh</p><button>Copiar</button></span> 
                <span><p>chmod 755 install.sh</p><button>Copiar</button></span> 
              </li>
              <li>
                Executar l’script install.sh i seguir les ordres.
                <span><p>./install.sh</p><button>Copiar</button></span>
              </li>
              <li>
                Obrir l’aplicació substituint el nom d’usuari:
                <span><p>http://labs.iam.cat/<span>~a16lydlaglag</span>/itvxml/index.php</p><button>Copiar</button></span> 
              </li>
            </ol>
          <h2>Passos a seguir per desinstal·lar l'aplicació</h2>
            <ol>
              <li>
                Anar al directori arrel del hosting i executar les següents comandes:
                <span><p>rm .htaccess</p><button>Copiar</button></span> 
                <span><p>rm -R public_html/itvxml</p><button>Copiar</button></span>                
              </li>
              <li>
                Esborrar la base de dades desde el teu panel de control.
              </li>
            </ol>
          </div>
        </div>
    </section>
      <script>
        $("button").click(function(){
          var $temp = $("<input>");
          $("body").append($temp);
          $temp.val($(this).prev().text()).select();
          document.execCommand("Copy");
          $temp.hide();
        });
      </script>
    </body>
</html>
