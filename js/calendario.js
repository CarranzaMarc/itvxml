var Calendario = (function(){
  var pintarCabecera = function(mes, ano){
    var tabla;
    var month = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    var m = month[mes];
    tabla="<tr class='container__mesany'><th colspan='4'>" + m + "</th><th class='container__any' colspan='3'>" + ano + "</th></tr><tr><th>Lun</th><th>Mar</th><th>Mie</th><th>Jue</th><th>Vie</th><th>Sab</th><th>Dom</th></tr>";
    return tabla;
  };
  
    
  
   var comprovarDias = function(mes){
     var diasmes;
     if(mes === 0 || mes == 2 || mes == 4 || mes == 6 || mes == 7 || mes == 9 || mes == 11){
      diasmes = 31;
     }
     else if(mes == 3 || mes == 5 || mes == 8 || mes == 10){
       diasmes = 30;
     }
     else{
       diasmes = 28;
     }
     return diasmes;
   };

   var pintarCalendario = function(dia, mes, ano){
     var fecha_actual;
     var tabla = "<table class='container__calendari'>" + pintarCabecera(mes, ano);
     var diasmes = comprovarDias(mes);
     var data = new Date();
     
     var myJSON = $("#citas_llenas").text(); 
     var citas_llenas = JSON.parse(myJSON);
     if(citas_llenas.length === 0){
       citas_llenas = "['0000-00-00']";
     }
     
     data.setDate(1);
     data.setMonth(mes); //los meses van de 0 a 11 si el usuario introduce 1 (enero) seria 0
     data.setFullYear(ano);
     d = data.getDay();
     d--;
     
     if (d === -1) {
       d = 6;
     }
     
     var num=1;
     for(var i=0;i<42;i++){
       if(i===0 || i==7 || i==14 || i==21 || i==28 || i==35){
         tabla+="<tr>";   //iniciamos una nueva fila
       }
       if(i<d || num>diasmes){
         tabla+="<td class='container__dia container__nodisponible'></td>";  //creamos una celda vacia
       }
       if(i>=d && num<=diasmes){
           mesreal = parseInt(mes) + 1;
           numform = "0" + num;
           mesrealform = "0" + mesreal;
           if(num < 10){
              if(mesreal < 10){
                fecha_actual = ano + "-" + mesrealform + "-" + numform;                 
              } else {
                fecha_actual = ano + "-" + mesreal + "-" + numform;
              }
           } else {
              if(mesreal < 10){
                fecha_actual = ano + "-" + mesrealform + "-" + num;                 
              } else {
                fecha_actual = ano + "-" + mesreal + "-" + num;
              }
           }
            
           var date = new Date();
           diahoy = date.getDate();
           anohoy = date.getFullYear();
           meshoy = date.getMonth() + 1;
           meshoyform = "0" + meshoy;
           diahoyform = "0" + dia;
         
           if(diahoy < 10){
            if(meshoy < 10){
                fecha_hoy = anohoy + "-" + meshoyform + "-" + diahoyform;
            } else {
              fecha_hoy = anohoy + "-" + meshoy + "-" + diahoyform;
            }
           } else {
            if(meshoy < 10){
              fecha_hoy = anohoy + "-" + meshoyform + "-" + diahoy;                 
            } else {
              fecha_hoy = anohoy + "-" + meshoy + "-" + diahoy;
            }
           }

         var disp = true;
         for(var k = 0; k < citas_llenas.length; k++){          
             if(fecha_actual == citas_llenas[k] || fecha_actual < fecha_hoy){
              disp = false;
             }
         }
         
         var domsab = new Date();
         domsab.setDate(num);
         domsab.setMonth(mes); //los meses van de 0 a 11 si el usuario introduce 1 (enero) seria 0
         domsab.setFullYear(ano);
         var ds = domsab.getDay();
         
         if(ds == 6 || ds === 0){
           disp = false;
         }
         
            if(disp){
              tabla+="<td class='container__dia container__disponible '>" + num + "</td>";   //creamos una celda con dia 
            } else {
              tabla+="<td class='container__dia container__nodisponible '>" + num + "</td>";   //creamos una celda con dia
            }
         
           num++;  
       }
       if(i==6 || i==13 || i==20 || i==27 || i==34 || i==41){
           tabla+="</tr>";  //cerramos la fila
       }
     }
     tabla += "</table>";
     return tabla;
   };
   
   return {
     pintarCalendario : pintarCalendario
   };

})();

   