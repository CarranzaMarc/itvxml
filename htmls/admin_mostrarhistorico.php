<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Admin ITV</title>
  <link rel="stylesheet" href="../css/normalize.css">
  <link rel="stylesheet" href="../css/adminbody.css">
  <link rel="stylesheet" href="../css/adminclient.css">
  <link rel="icon" href="../imgs/favicon.png">
  <script src="../js/jquery.js"></script>
  <script src="../js/calendario.js"></script>
</head>

<body>
  <?php
    require_once('../js/adminheader.php');
  ?>
  <?php
    require_once('../js/adminnav.php');
  ?>
  <section class="container">
      <div class="container__objects">
        <div class="container__wrapper">
           <?php
              require_once '../utils/login.php';
              $db_server = mysqli_connect($db_hostname, $db_username, $db_password, $db_database);
              if (!$db_server) die("Unable to connect to MySQL: " . mysql_error());    
              mysqli_select_db($db_server, $db_database) or die("Unable to select database: " . mysqli_error());
              $query = "INSERT INTO historico SELECT id, fecha, hora, matricula, id_centro FROM citas WHERE fecha < CURDATE();";
              $result = mysqli_query($db_server, $query);         
              $query = "DELETE FROM citas WHERE fecha < CURDATE();";
              $result = mysqli_query($db_server, $query);    
              $query = "SELECT id, fecha, hora, matricula FROM historico;"; 
              $result = mysqli_query($db_server, $query);
              if (!$result) die ("Database access failed: " . mysql_error()); 
              $rows = mysqli_num_rows($result);
              echo "<table class='container__calendari'><tr class='container__thead'><th>ID</th><th>Data</th><th>Hora</th><th>Matricula</th></tr>";
              for ($i = 0 ; $i < $rows ; $i++){
                echo "<tr>";
                $consulta = mysqli_fetch_assoc($result);
                foreach ($consulta as $key => $valor) {
                  echo "<td>".$valor."</td>";
                }
                echo "</tr>";
              }
              echo "</table>";
              mysqli_close($db_server);
           ?>
           <a href="adminpanell.php">
              <button class="container__btnback">
                Tornar
              </button>
            </a>
        </div>
      </div>
 
    </section>
</body>

</html>