<?php
  $footer = "<footer class='footer'>";
  $footer .= "<div class='block'>";
  $footer .= "<div class='block-left'>";
  $footer .= "<ul class='block-left__list'>";
  $footer .= "<li class='block-left__li'><a href='#'>Mapa web</a></li>";
  $footer .= "<li class='block-left__li'><a href='#'>Avís legal</a></li>";
  $footer .= "<li class='block-left__li'><a href='#'>Ajuda</a></li>";
  $footer .= "<li class='block-left__li'><a href='#'>Contacte</a></li>";
  $footer .= "</ul></div>";
  $footer .= "<div class='block-right'>";
  $footer .= "<ul class='block-right__list'>";
  $footer .= "<li class='block-right__li'><a href='#'>www.iamITV.cat</a></li>";
  $footer .= "<li class='block-right__li'><span class='block-right__span'>© IAM ITV, 2K17</span></li>";
  $footer .= "</ul></div></div></footer>";
  echo $footer;
?>