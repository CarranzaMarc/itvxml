#!/bin/bash
cd public_html
git clone https://lydialago@bitbucket.org/CarranzaMarc/itvxml.git
mv itvxml/.htaccess ./
chmod 755 .htaccess
chmod 755 -R itvxml
sed 's/\sDEFINER=`[^`]*`@`[^`]*`//' -i itvxml/db_scripts/itv.sql
echo "Introdueix el nom de la base de dades:"
read nombd
echo "Introdueix l'usuari de la base de dades:"
read userbd
echo "Introdueix la contrassenya de la base de dades:"
read passbd
echo "Configurant la base de dades..."
touch itvxml/utils/login.php
chmod 755 itvxml/utils/login.php
echo "
<?php
	\$db_hostname = 'localhost';
	\$db_database = '$nombd';
	\$db_username = '$userbd';
	\$db_password = '$passbd';
?>
" > itvxml/utils/login.php
mysql -u $userbd -p $nombd < itvxml/db_scripts/itv.sql
echo "Fet!"
