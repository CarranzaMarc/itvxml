<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <META HTTP-EQUIV="refresh" CONTENT="3; url=../index.php" >
  <title>IAM ITV</title>
  <link rel="stylesheet" href="../css/normalize.css">
  <link rel="stylesheet" href="../css/body.css">
  <link rel="stylesheet" href="../css/cancelar.css">
  <link rel="icon" href="../imgs/favicon.png">
</head>

<body>
  <?php
    require_once('../js/header.php');
  ?>
  <section class="container">
    <div class="container__objects">
      <p class="container__text">
        La teva cita s'ha eliminat correctament.
      </p>
      <p class="container__subtext">
        Seràs redirigit a la pàgina principal en 5 segons
      </p>
    </div>
  </section>
  <?php
    require_once('../js/footer.php');
  ?>
</body>
</html>