<?php
session_start();
function obtener_error ($n){
  switch($n){
    case 0:
      $_SESSION['error'] = "No s'ha pogut accedir a la base de dades";
      break;
    case 1:
      $_SESSION['error'] = "Aquesta hora ja no està disponible";
      break;     
    case 2:
      $_SESSION['error'] = "Les dades no són correctes";
      break; 
    case 3:
      $_SESSION['error'] = "No és un error, és un miracle Nadalenc";
      break;
  }
  header("Location: ../htmls/errorpage.php");
}
?>

<!-- 0 acceso base datos
1 no insert
2 campos vacios 
3 error por defecto
-->
