<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>IAM ITV</title>
  <link rel="stylesheet" href="../css/normalize.css">
  <link rel="stylesheet" href="../css/body.css">
  <link rel="icon" href="../imgs/favicon.png">
</head>

<body>
  <?php
    require_once('../js/header.php');
  ?>
  <section class="container">
    <div class="container__objects">
    </div>
  </section>
  <?php
    require_once('../js/footer.php');
  ?>
</body>

</html>