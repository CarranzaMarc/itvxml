#CASOS D'ÚS

##index.php
L'usuari introdueix la matricula. Al client es comprova si el camp no està buit i la informació introduida correspon a una matrícula. Al servidor es comprova si la matricula esta a la base de dades (taula vehiculos). 
*Si el vehicle SI està a la base de dades (taula citas), es mostra gestio.php amb la info de la cita i dos botons de modificar i eliminar cita. No es permet reservar més d'una cita per matrícula*
*Si el vehicle NO està a la base de dades (taula citas), es passa a mostrar el calendari. La matricula es guarda en una variable $_SESSION['matricula']*

##calendari.php
Es comprova quins dies tenen hores disponibles. Els dies no disponibles no es poden seleccionar. Quan l'usuari selecciona un dia es passa directament a la següent pàgina. La data es guarda en una variable $_SESSION['fecha'].

##hora.php
Es mostra un llistat amb les hores disponibles d'aquella data. Les hores no disponibles no es poden seleccionar. Per seleccionar un altre dia l'usuari ha d'anar a la pàgina anterior. La hora es guarda en una variable $_SESSION['hora']. 

##confirmar.php
Se li demanen les dades a l'usuari i donem l'opció de confirmar o editar. Abans de fer un insert a la taula citas cal fer dos inserts més: un amb les dades del client a la taula clientes i un altre amb les dades del vehicle a la taula vehichulos
1) Esborrar qualsevol cita de la taula citas amb aquesta matricula (a tenir en compte en el cas de modificació de la cita)
2) Esborrar de la taula vehicles el vehicle amb la matricula introduida 
(si la persona X amb una cita ven el coche a la persona Y, si aquesta ultima vol demanar cita se li enviarà a modificar cita. Confirmant la cita,
s'esborraran les dades de la taula vehicles amb l'email de la persona X i s'inseriran les dades amb l'email de la persona Y. No té sentit mantenir
les dades del vehicle amb el email de la persona X perque ja no és el propietari i no passara la ITV).
3) Esborrar de la taula clientes el client amb l'email introduit per si ha canviat alguna dada (p.ex. el telèfon) però l'email és el mateix 
(al ser l''email primary key fer l'insert directament ens donaria error).
Si clica a editar es torna al calendari, si clica a confirmar se li mostra la informació de la cita

##informacio.php
Es mostra tota la informació de la cita amb un link per tornar a l'index.

##gestio.php
Si clica a modificar cita es torna al calendari per escollir data i hora. Es tornen a demanar les dades en comptes de mostrar-les als camps corresponents (aixi evitem que una altra persona que coneix la matrícula tingui les dades del propietari).
Si clica a eliminar es demana la confirmació i es mostra una pàgina que redirigeix a l'index.
