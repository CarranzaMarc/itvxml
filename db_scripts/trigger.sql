delimiter //
CREATE TRIGGER `listado_citas` AFTER INSERT ON `citas`
 FOR EACH ROW BEGIN 
DECLARE a INT;
SELECT COUNT(id) INTO a FROM citas WHERE fecha = NEW.fecha;
IF a = 48 THEN 
INSERT INTO citas_llenas SET fecha = NEW.fecha;
END IF;
END//
delimiter ;

delimiter //
CREATE TRIGGER `citas_multiples` BEFORE INSERT ON `citas`
 FOR EACH ROW BEGIN 
DECLARE n INT;
DECLARE correu VARCHAR(70);
SELECT COUNT(id) INTO n FROM citas 
WHERE fecha LIKE NEW.fecha
AND hora LIKE NEW.hora;
IF (n >= 2) THEN
CALL you_shall_not_pass;
END IF;
END//
delimiter ;

delimiter //
CREATE TRIGGER `update_lista_citas` AFTER DELETE ON `citas`
FOR EACH ROW BEGIN
DECLARE a INT;
SELECT COUNT(id) INTO a FROM citas WHERE fecha = OLD.fecha;
IF a < 48 THEN 
DELETE FROM citas_llenas WHERE fecha = OLD.fecha;
END IF;
END//
delimiter;
