<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>IAM ITV</title>
  <link rel="stylesheet" href="../css/normalize.css">
  <link rel="stylesheet" href="../css/body.css">
  <link rel="stylesheet" href="../css/gestio.css">
  <link rel="icon" href="../imgs/favicon.png">
  <script src="../js/jquery.js"></script>
</head>

<body>
  <?php
    require_once('../js/header.php');
    session_start();
  ?>
    <section class="container">
      <div class="container__objects">
        <div class="container__data">
          <h1 class="container__title">
            RESERVA
          </h1>
          <table class="container__table">
            <tr>
              <td>Data:</td>
              <td class="container__important"><?php echo $_SESSION['fecha']; ?></td>
            </tr>
            <tr>
              <td>Hora:</td>
              <td class="container__important"><?php echo $_SESSION['hora']; ?></td>
            </tr>
            <tr>
              <td>Matricula:</td>
              <td class="container__important"><?php echo $_SESSION['matricula']; ?></td>
            </tr>
            <tr>
              <td>Centre:</td>
              <td class="container__important"><?php echo $_SESSION['centro']; ?></td>
            </tr>
            <tr>
              <td class="container__btns" colspan="2">
                <a href="calendari.php">
                  <button type="submit" class="container__btn">Modificar</button>
                </a>
                <a href="">
                  <button id="cancelar" type="submit" class="container__btn">Anular</button>
                </a>
              </td>
            </tr>
          </table>
        </div>
      </div>
    </section>
    <?php
      require_once('../js/footer.php');
    ?>
</body>
  <script>  
    $('#cancelar').click(function(){
      if(confirm("Segur que vols eliminar aquesta cita?") == true){
        $(this).parent().attr("href", "../utils/anular_cita.php");
      }      
      else{
       return false;
      }
    });
</script>
</html>