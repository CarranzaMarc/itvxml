<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>IAM ITV</title>
  <link rel="stylesheet" href="../css/normalize.css">
  <link rel="stylesheet" href="../css/errorpage.css">
  <link rel="icon" href="../imgs/favicon.png">
</head>

<body>
  <section class="container">
    <div class="container__objects">
      <span class="container__errortext">
        <?php
          session_start();
          echo $_SESSION['error'];    
        ?>
      </span>
      <a href="../index.php">
        <button class="container__btn">
          Tornar a la pantalla principal
        </button>
      </a>
    </div>
  </section>
</body>

</html>