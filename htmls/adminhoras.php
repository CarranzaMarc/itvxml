<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>IAM ITV</title>
  <link rel="stylesheet" href="../css/normalize.css">
  <link rel="stylesheet" href="../css/adminbody.css">
  <link rel="stylesheet" href="../css/adminhoras.css">
  <link rel="icon" href="../imgs/favicon.png">
  <script src="../js/jquery.js"></script>
  <script src="../js/adminhoras.js"></script>
</head>

<body>
  <?php
    require_once('../js/adminheader.php');
  ?>
  <?php
    require_once('../js/adminnav.php');
  ?>
  <?php
  session_start();
  if(isset($_POST["fecha"])){
    $_SESSION["fecha"] = $_POST["fecha"];
  }
   
  require_once '../utils/login.php';

  $db_server = mysqli_connect($db_hostname, $db_username, $db_password, $db_database);
  if (!$db_server) die("Unable to connect to MySQL: " . mysql_error());    

  mysqli_select_db($db_server, $db_database) or die("Unable to select database: " . mysqli_error());
  
   $query = "SELECT hora, matricula FROM citas WHERE fecha like '".$_POST['fecha']."';";
    $result = mysqli_query($db_server, $query);
    if (!$result) die ("Database access failed: " . mysql_error());
    $rows = mysqli_num_rows($result);
    $json = array();
    for ($i = 0 ; $i < $rows ; $i++){
      $consulta = mysqli_fetch_assoc($result);
      foreach ($consulta as $key => $valor) 
        $json[] = $valor;
    }

  
    $json = json_encode($json);
    print "<div class='hide' id='horas_ocupadas'>" . $json . "</div>";
  ?>
 <section class="container">
   <div class="container__objects">
     <div class="container__wrapper">
       <h2 class="container__title"><?php echo $_POST['fecha'] ?></h2>
       <div class="container__tables">
         <table id="printHere" class="container__calendari"></table>
         <table id="printHere2" class="container__calendari"></table>
         <table id="printHere3" class="container__calendari"></table>
       </div>
    </div>
  </section>

  
</body>

</html>
