DROP DATABASE ITV
CREATE DATABASE ITV;
USE ITV;

CREATE TABLE clientes (
    email VARCHAR(70) PRIMARY KEY,
    nombre VARCHAR(30) NOT NULL,
    apellidos VARCHAR(70) NOT NULL,
    telefono VARCHAR(12) NOT NULL
   )ENGINE=InnoDB;


CREATE TABLE vehiculos (
    matricula VARCHAR(8) PRIMARY KEY,
    tipo_vehiculo ENUM ('coche', 'moto', 'camion', 'tractor', 'autobus', 'ambulancia'),
    email_cliente VARCHAR(70) NOT NULL,
    FOREIGN KEY (email_cliente) REFERENCES clientes (email)
   )ENGINE=InnoDB;

CREATE TABLE centros (
    id INT PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(70) NOT NULL,
    direccion VARCHAR(100) NOT NULL,
    nlineas_coche TINYINT(2) NOT NULL,
    nlineas_moto TINYINT(2) NOT NULL
    )ENGINE=InnoDB;

CREATE TABLE citas (
    id INT PRIMARY KEY AUTO_INCREMENT,
		fecha DATE,
		hora TIME,
    matricula VARCHAR(8),
    id_centro INT,
    FOREIGN KEY (matricula) REFERENCES vehiculos (matricula),
    FOREIGN KEY (id_centro) REFERENCES centros (id)
)ENGINE=InnoDB;

CREATE TABLE historico (
    id INT PRIMARY KEY AUTO_INCREMENT,
		fecha DATE,
		hora TIME,
    matricula VARCHAR(8),
    id_centro INT
)ENGINE=InnoDB;
	    
CREATE TABLE citas_llenas (
	fecha DATE PRIMARY KEY
)ENGINE=InnoDB;
