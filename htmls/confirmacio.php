<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>IAM ITV</title>
  <link rel="stylesheet" href="../css/normalize.css">
  <link rel="stylesheet" href="../css/body.css">
  <link rel="stylesheet" href="../css/confirmacio.css">
  <link rel="icon" href="../imgs/favicon.png">
  <script src="../js/jquery.js"></script>
</head>

<body>
  <?php
    require_once('../js/header.php');
    session_start();
  
    if(isset($_POST["hora"])){
      $_SESSION["hora"] = $_POST["hora"];
    }
  ?>
    <section class="container">
      <div class="container__objects">
        <!--WRAPPER PER EL GRID -->
        <div class="container__wrapper">
          <!-- DIV ON VAN LES DADES DE LA CITA -->
          <div class="container__data">
            <h1 class="container__title">
              RESERVA
            </h1>
            <table class="container__table">
              <tr>
                <td class="container__name">Data:</td>
                <td class="container__important">
                  <input value="<?php echo $_SESSION['fecha']; ?>" disabled>
                </td>
              </tr>
              <tr>
                <td class="container__name">Hora:</td>
                <td class="container__important">
                  <input value="<?php echo $_SESSION['hora']; ?>" disabled>
                </td>
              </tr>
              <tr>
                <td class="container__name">Matricula:</td>
                <td class="container__important">
                  <input value="<?php echo $_SESSION['matricula']; ?>" disabled>
                </td>
              </tr>
              <tr>
                <td class="container__name">Centre:</td>
                <td class="container__important">
                  <input value="<?php echo $_SESSION['centro']; ?>" disabled>
                </td>
              </tr>
            </table>
          </div>
          <!-- DIV PER LES DADES DEL FORMULARI -->
          <div class="container__form">
            <form id="form" method="POST" action="../utils/insertar_dades.php">
              <h1 class="container__title">
                DADES
              </h1>
              <table class="container__table">
                <tr>
                  <td class="container__name">Nom: </td>
                  <td><input required class="container__formcamp" type="text" name="nom" placeholder="Nom" maxlength="30"></td>
                </tr>
                <tr>
                  <td class="container__name">Cognoms: </td>
                  <td><input required class="container__formcamp" type="text" name="cognoms" placeholder="Cognoms" maxlength="70"></td>
                </tr>
                <tr>
                  <td class="container__name">Email: </td>
                  <td><input required class="container__formcamp" type="email" name="email" placeholder="Email" maxlength="70"></td>
                </tr>
                <tr>
                  <td class="container__name">Telèfon: </td>
                  <td><input required class="container__formcamp" type="tel" name="telefon" placeholder="Telefon"></td>
                </tr>
              </table>
            </form>
          </div>
          <!-- DIV PER ALS BOTONS -->
          <div class="container__btns">
            <a href="calendari.php">
              <input type="button" class="container__btn" value="Editar">
            </a>
            <a>
              <input id="submit" type="submit" form="form" class="container__btn" value="Confirmar">
            </a>
          </div>
        </div>
    </section>
    <?php
    require_once('../js/footer.php');
    ?>
    <script>
      $('input[name="telefon"]').focus();
        if($('input[name="telefon"]').is(':empty')){
          $('#submit').prop('disabled', true);
        }
        var error = false;
        $('input[name="telefon"]').on('input', function(){
        var expreg = /^(\+34|0034|34)?[6|7|9][0-9]{8}$/;    
        if($(this).val().search(expreg)){  
          $(this).css("border", "1px solid red");
          $(this).css("background", "rgba(255, 0, 0, 0.5)");
          
          error = true;
          $('#submit').prop('disabled', true);
        }
        else {
          $(this).css("border",  "");
          $(this).css("background", "");
          
          $('#submit').prop('disabled', false);
        }
      });
    
       $("#submit").click(function(){
         if(confirm("Segur que vols aquesta cita?") == true){
            $("#form").submit();
         }      
         else{
           return false;
         }
       });
      
    </script>
</body>

</html>